import java.util.*;
public class CollectionsDemo {
    public static void main(String[] args) {
        List<String> arrayList = new ArrayList<>();
        arrayList.add("Apple");
        arrayList.add("Banana");
        arrayList.add("Orange");
        System.out.println("ArrayList:");
        for (String fruit : arrayList) {
            System.out.println(fruit);
        }
        System.out.println();
        Set<String> hashSet = new HashSet<>();
        hashSet.add("Red");
        hashSet.add("Green");
        hashSet.add("Blue");
        hashSet.add("Green"); 
        System.out.println("HashSet:");
        for (String color : hashSet) {
            System.out.println(color);
        }
        System.out.println();
        Map<String, Integer> hashMap = new HashMap<>();
        hashMap.put("John", 25);
        hashMap.put("Jane", 30);
        hashMap.put("Adam", 28);


        System.out.println("HashMap:");
        for (Map.Entry<String, Integer> entry : hashMap.entrySet()) {
            String name = entry.getKey();
            int age = entry.getValue();
            System.out.println(name + " - " + age + " years old");
        }
    }
}
