import java.awt.*;
import java.awt.event.*;
 
public class LoginPage extends Frame implements ActionListener {
   private Label titleLabel, usernameLabel, passwordLabel, messageLabel;
   private TextField usernameField, passwordField;
   private Button loginButton, cancelButton;
 
   public LoginPage() {
      setLayout(new GridLayout(4,2));
      
      titleLabel = new Label("Login Page");
      add(titleLabel);
      add(new Label(""));
      
      usernameLabel = new Label("Username:");
      add(usernameLabel);
      usernameField = new TextField(20);
      add(usernameField);
      
      passwordLabel = new Label("Password:");
      add(passwordLabel);
      passwordField = new TextField(20);
      passwordField.setEchoChar('*');
      add(passwordField);
      
      loginButton = new Button("Login");
      add(loginButton);
      loginButton.addActionListener(this);
      
      cancelButton = new Button("Cancel");
      add(cancelButton);
      cancelButton.addActionListener(this);
      
      messageLabel = new Label("");
      add(messageLabel);
      add(new Label(""));
      
      setTitle("Login Page");
      setSize(300, 200);
      setVisible(true);
   }
   
   public void actionPerformed(ActionEvent e) {
      if (e.getSource() == loginButton) {
         String username = usernameField.getText();
         String password = passwordField.getText();
         if (username.equals("admin") && password.equals("password")) {
            messageLabel.setText("Login successful");
         } else {
            messageLabel.setText("Invalid username or password");
         }
      } else if (e.getSource() == cancelButton) {
         usernameField.setText("");
         passwordField.setText("");
         messageLabel.setText("");
      }
   }
   
   public static void main(String[] args) {
      new LoginPage();
   }
