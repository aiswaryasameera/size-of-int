import java.io.*;
public class CpCommandExample {
public static void main(String[] args) throws IOException {
if (args.length != 2) {
System.out.println("Usage: java CpCommandExample <source_file_path>
<destination_file_path>");
return;
}
File sourceFile = new File(args[0]);
File destinationFile = new File(args[1]);
if (!sourceFile.exists()) {
System.out.println("Source file doesn't exist!");
return;
}
if (destinationFile.exists()) {
System.out.println("Destination file already exists!");
return;
}
FileInputStream inputStream = null;
FileOutputStream outputStream = null;
try {
inputStream = new FileInputStream(sourceFile);
outputStream = new FileOutputStream(destinationFile);
byte[] buffer = new byte[1024];
int length;
while ((length = inputStream.read(buffer)) > 0) {
outputStream.write(buffer, 0, length);
}
System.out.println("File copied successfully!");
} finally {
if (inputStream != null) {
inputStream.close();
}
if (outputStream != null) {
outputStream.close();
}
}
}
}
