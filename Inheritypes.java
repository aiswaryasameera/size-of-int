import java.lang.*;


//parent class
class Demo1 {
    int a = 10;


    void display1() {
        System.out.println("content in Demo1 is : " + a);
    }
}


// simple inheritance
class Demo2 extends Demo1 {
    int b = 20;


    void display2() {
        System.out.println("content in Demo2 is : " + (a + b));
    }
}


// multilevel inheritance
class Demo3 extends Demo2 {
    int c = 30;


    void display3() {
        System.out.println("content in Demo3 is : " + (b + c));
    }
}


// hierarchical inheritance
class Demo4 extends Demo1 {
    int d = 40;


    void display4() {
        System.out.println("content in Demo4 is : " + (a + d));
    }
}


// hybrid inheritance
class Demo5 extends Demo2 {
    int e = 50;


    void display5() {
        System.out.println("content in Demo5 is : " + (b + e));
    }
}


class InheriTypes {
    public static void main(String[] args) {
        Demo2 obj2 = new Demo2();// simple inheritance
        obj2.display2();
        Demo3 obj3 = new Demo3();// multilevel inheritance
        obj3.display3();
        Demo4 obj4 = new Demo4();// hierarchical inheritance
        obj4.display4();
        Demo5 obj5 = new Demo5();// hybrid inheritance
        obj 5.display 5();
    }
}




