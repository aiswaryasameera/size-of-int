import java.util.*;
public class MapInterfaceDemo {
    public static void main(String[] args) {
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "John");
        map.put(2, "Jane");
        map.put(3, "Adam");
        System.out.println("Value at key 1: " + map.get(1));
        System.out.println("Value at key 2: " + map.get(2));
        System.out.println("Value at key 3: " + map.get(3));
        map.put(1, "Sarah");
        System.out.println("Updated value at key 1: " + map.get(1));
        map.remove(2);
        System.out.println("Map after removing key 2: " + map);
        System.out.println("Contains key 3: " + map.containsKey(3));
        System.out.println("Contains value 'Adam': " + map.containsValue("Adam"));
        System.out.println("Size of the map: " + map.size());
        System.out.println("Map entries:");
        for (Map.Entry<Integer, String> entry : map.entrySet()) {
            int key = entry.getKey();
            String value = entry.getValue();
            System.out.println(key + " - " + value);
        }
        map.clear();
        System.out.println("Map after clearing: " + map);
    }
}
