class AgeException extends Exception {
public AgeException(String message) {
super(message);
}
}
class Person {
private String name;
private int age;
public Person(String name, int age) throws AgeException {
if (age < 0 || age > 120) {
throw new AgeException("Age should be between 0 and 120");
}
this.name = name;
this.age = age;
}
public void display() {
System.out.println("Name: " + name);
System.out.println("Age: " + age);
}
}
public class Demo {
public static void main(String[] args) {
try {
Person p = new Person("Fhareh", 130);
p.display();
} catch (AgeException e) {
System.out.println(e.getMessage());
}
}
}
